# Run on EC2
```
# Create new instance t2.large 4CPU 16G Ram
Edit security group to expose port 8081 and 8082 (inbound rules)
ssh -i "~/.ssh/longvu.key.ec2.pem" ubuntu@ec2-13-229-216-167.ap-southeast-1.compute.amazonaws.com

# Create new instance t2.large 2CPU 8G Ram
ssh -i "~/.ssh/longvudeveleap.pem" ubuntu@35.153.153.167

sudo apt-get update -y && sudo apt-get upgrade -y

# Install docker
sudo apt-get install ca-certificates curl gnupg lsb-release -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

# Fix permission
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
docker run hello-world
sudo chmod 666 /var/run/docker.sock
#sudo reboot

# Install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version

# Clone source code
mkdir -p ~/data
mkdir -p ~/data/gitlab
mkdir -p ~/data/jenkins_home
cd ~/data
git clone https://gitlab.com/longersoft-public/testing/install-jenkins.git
cd <inside>
docker-compose build
sudo docker-compose up -d
docker-compose logs -f

# Setup jenkins
http://13.229.216.167:8081
docker container exec -it jenkins bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
2987105545c74e9199d503a317e53cc6

## Instal plugins:
Docker
Docker Pipeline
AWS ...
Gitlab
SSH Agent
Artifactory

## Setup pipeline:
cowsay_freestyle
cowsay_pipeline
cowsay_versioning

## Create credentials:
User/pass: gitlabuser
User/ssh: gitlabuser-ssh
AWS Credentials

## Setting JFrog
Add JFrog Instance -> ID: frog-artifactory
Platform URL: http://35.153.153.167:8083

Generate Artifactory Secret:
http://35.153.153.167:8084/ui/admin/configuration/security/access_tokens
Then, add Jenkins secret text
http://35.153.153.167:8081/credentials/store/system/domain/_/newCredentials
Add new local Repository
http://35.153.153.167:8084/ui/admin/repositories/local/new


# Setup gitlab
http://54.196.132.134:8082
root/
user/gitlabuser
docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
Password: N7QBHlxQOvwxR1QYhXsQ/aSYtR16Y2MsEa//BkD4dvo=
docker exec -it gitlab bash
vi /etc/gitlab/gitlab.rb
>> edit: external_url 'http://54.196.132.134:8082'
gitlab-ctl reconfigure
or:
docker-compose restart gitlab

# Set SSH Key
Goto: http://35.153.153.167:8082/-/profile/keys


change pass: https://stackoverflow.com/questions/55747402/docker-gitlab-change-forgotten-root-password
docker exec -it d0bbe0e1e3db bash <-- with your CONTAINER_ID
gitlab-rails console -e production
user = User.where(id: 1).first
user.password = 'your secret'
user.password_confirmation = 'your secret'
user.save
exit


# Setup JFrog artifactory
http://35.153.153.167:8083
admin/password

```

# CICD
```
Git repo should be:
http://user:gitlabuser@35.153.153.167:8082/user/cowsay.git

Another way:
ssh-agent bash -c 'ssh-add ~/.ssh/longvudeveleap.pem; git clone ssh://git@54.196.132.134:8422/user/cowsay.git'
ssh-agent bash -c 'ssh-add ~/.ssh/longvudeveleap.pem; git push'

```

# Jenkin Docker build
```
Building containers
https://www.jenkins.io/doc/book/pipeline/docker/
https://stackoverflow.com/a/69180266
```

# Check container ready
https://stackoverflow.com/questions/31746182/docker-compose-wait-for-container-x-before-starting-y
https://stackoverflow.com/questions/4922943/test-if-remote-tcp-port-is-open-from-a-shell-script
https://stackoverflow.com/questions/38576337/how-to-execute-a-bash-command-only-if-a-docker-container-with-a-given-name-does
```
if [ "$(docker ps -q -f name=cowsay)" ]; then
    if [ "$(docker ps -aq -f status=exited -f name=cowsay)" ]; then
        curl localhost:4001
    fi
fi
```

# Maven commands
https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html#running-maven-tools
validate: validate the project is correct and all necessary information is available
compile: compile the source code of the project
test: test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
package: take the compiled code and package it in its distributable format, such as a JAR.
integration-test: process and deploy the package if necessary into an environment where integration tests can be run
verify: run any checks to verify the package is valid and meets quality criteria
install: install the package into the local repository, for use as a dependency in other projects locally
deploy: done in an integration or release environment, copies the final package to the remote repository for sharing with other developers and projects.
There are two other Maven lifecycles of note beyond the default list above. They are

clean: cleans up artifacts created by prior builds
site: generates site documentation for this project

## Elastic IP (static IP)
https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Addresses:

# Docker get container name inside container
https://medium.com/disassembly/get-container-name-inside-the-docker-container-1997d36aaecd

# Docker access host-port from container
https://stackoverflow.com/questions/31324981/how-to-access-host-port-from-docker-container
```
https://forums.docker.com/t/accessing-host-machine-from-within-docker-container/14248/4
docker network create -d bridge --subnet 192.168.0.0/24 --gateway 192.168.0.1 dockernet
```

## Jenkins convert freestyle -> Jenkinfile
https://docs.cloudbees.com/docs/admin-resources/latest/pipelines/declarative-pipeline-migration-assistant

## Jenkins enable pipeline
https://youtu.be/s73nhwYBtzE?t=307

## Jenkins build on specific branch
https://stackoverflow.com/questions/32108380/jenkins-how-to-build-a-specific-branch
Add Git Parameter BRANCH here This project is parameterized
Install plugin Git Parameter



## Jenkins send email notification
https://www.learnitguide.net/2018/08/how-to-configure-jenkins-mail.html
https://www.educba.com/jenkins-email-notification/

## Jenkins to ECR
https://plugins.jenkins.io/amazon-ecr/
https://betterprogramming.pub/how-to-push-a-docker-image-to-amazon-ecr-with-jenkins-ed4b042e141a

## Jenkins artifact
https://www.jenkins.io/doc/pipeline/examples/

## JFrog Upload and Download files
https://www.jfrog.com/confluence/display/CLI/CLI+for+JFrog+Artifactory

## Jenkins force close pipeline
https://stackoverflow.com/questions/14456592/how-to-stop-an-unstoppable-zombie-job-on-jenkins-without-restarting-the-server
Manage -> Script Console
Run:
Jenkins .instance.getItemByFullName("cowsay_pipeline")
        .getBuildByNumber(14)
        .finish(hudson.model.Result.ABORTED, new java.io.IOException("Aborting build")); 

## Jenkins -> Gitlab Webhook
https://stackoverflow.com/questions/55693706/gitlab-jenkins-webhook-integration
https://docs.gitlab.com/ee/integration/jenkins.html
http://35.153.153.167:8082/-/profile/personal_access_tokens
Create: Personal Access Tokens
jenkin-gitlab / pfkEF2r8-npGsbvJYXLa

Jenkin: secret: 36a29d978a5fa63012d7225b96cf105a

## Jenkins indicate Gitlab
https://github.com/jenkinsci/gitlab-plugin#scripted-pipeline-jobs

## Jenkins build docker image and push to ECR
https://stackoverflow.com/questions/59084989/push-to-ecr-from-jenkins-pipeline

## Jenkins use SSH Key
https://stackoverflow.com/questions/44237417/how-do-i-use-ssh-in-a-jenkins-pipeline

## Jenkins, EC2 login, pull image from ECR
https://forums.docker.com/t/pull-images-from-aws-ecr-on-aws-ec2-without-using-docker-login-but-using-ec2-instance-role-and-ecr-repository-permissions/39863/2

## Install JFrog Artifactory
https://computingforgeeks.com/how-to-install-jfrog-artifactory-on-centos/
https://github.com/JFrog/artifactory-docker-examples

## Jenkins and JFrog Artifactory
https://www.youtube.com/watch?v=GE_AsQ6jy84
https://www.jfrog.com/confluence/display/JFROG/Configuring+Jenkins+Artifactory+Plug-in
https://www.youtube.com/watch?v=fj_TD9pufFM
https://www.youtube.com/watch?v=AlaRlwCekY0


# Temp
```
docker build --no-cache -t jenkins -f jenkins/Dockerfile .
docker run --rm -it --name=jenkins jenkins bash
docker build --no-cache -t gitlab -f gitlab/Dockerfile .
docker run --rm -it --name=jenkins jenkins bash

docker exec -it gitlab bash
docker exec -it gitlab gitlab-ctl status

apt update && apt install lsof && \
lsof -i -P -n | grep LISTEN

docker rmi gitlab --force
docker build -t gitlab -f gitlab/Dockerfile .

docker rmi hello-world --force
docker rmi gitlab/gitlab-ce
docker rmi jenkins/jenkins:lts

docker stop gitlab && docker rm gitlab
docker run --detach \
  --name gitlab \
  --restart always \
  --publish 8082:80 \
  --publish 8443:443 \
  --publish 8422:22 \
  --volume ${pwd}/gitlab/config:/etc/gitlab \
  --volume ${pwd}/gitlab/logs:/var/log/gitlab \
  --volume ${pwd}/gitlab/data:/var/opt/gitlab \
  --shm-size 256m \
  gitlab
docker logs -f gitlab

## Error:
> docker: Error response from daemon: Mounts denied: 
>> sudo setfacl -mR default:group:docker:rwx ${pwd}/gitlab

> Whoops, GitLab is taking too much time to respond.
https://forum.gitlab.com/t/502-whoops-gitlab-is-taking-too-much-time-to-respond/52522/4
>> 4 CPU and 4GB RAM minimum requiremts
>> wait for the log production.log ... around 4-5m
>> check log and see this
gitlab     | 2022-02-15_18:00:48.95311 Errno::EADDRINUSE: Address already in use - bind(2) for "127.0.0.1" port 8080
>>> change port for gitlab

> Unable to connect to Jenkins
>> wait, retry

> Invalid agent type "docker" specified. Must be one of [any, label, none] @ line 3, column 13.
>> You have to install 2 plugins: Docker plugin and Docker Pipeline. Hope that helps.
(https://stackoverflow.com/questions/62253474/jenkins-invalid-agent-type-docker-specified-must-be-one-of-any-label-none)



https://docs.gitlab.com/ee/install/docker.html

sudo docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume $GITLAB_HOME/config:/etc/gitlab \
  --volume $GITLAB_HOME/logs:/var/log/gitlab \
  --volume $GITLAB_HOME/data:/var/opt/gitlab \
  --shm-size 256m \
  gitlab/gitlab-ee:latest

# Cronj
https://www.golinuxcloud.com/create-schedule-cron-job-shell-script-linux/
https://askubuntu.com/questions/567955/automatic-shutdown-at-specified-times
```