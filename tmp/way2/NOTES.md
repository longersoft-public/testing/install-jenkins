## https://itnext.io/docker-inside-docker-for-jenkins-d906b7b5f527

Adapting Jenkins container
First, we need to create our own image for our jenkins container. Here is Dockerfile:
```
FROM jenkins/jenkins
USER root
RUN apt-get -y update && \
 apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add — && \
 add-apt-repository \
 “deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo “$ID”) \
 $(lsb_release -cs) \
 stable” && \
 apt-get update && \
 apt-get -y install docker-ce docker-ce-cli containerd.io
RUN curl -L “https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)” -o /usr/local/bin/docker-compose && \
 chmod +x /usr/local/bin/docker-compose && \
 ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
USER jenkins
```
First, we declare our base image. Next, we switch to the root user. The image which we based on does not contain docker, so we install it. In the end, we switch to jenkins user to use it by default. Now we need to use that image to run our container. We’ll use docker-compose for that. Here is our docker-compose.yml:
```
version: '3.1'
services:
    jenkins:
        build:
            context: ./
        restart: unless-stopped
        volumes:
            - ${HOST_DOCKER}:/var/run/docker.sock
            - ${HOST_JENKINS_DATA}:/var/jenkins_home
        ports:
            - "${HOST_WWW}:8080"
            - "${HOST_OTHER}:50000"
```
Now let’s finish everything with the configuration of environments variables for docker with .env file:
```
HOST_WWW=8180
HOST_OTHER=8181
HOST_DOCKER=/var/run/docker.sock
HOST_JENKINS_DATA=/srv/www/jenkins
```
As you see on the host, we’ll have /srv/www/jenkins, which will be mounted on the container as /var/jenkins_home. This gives us the possibility to preserve all Jenkins configuration when we restart the container. Port 8180 on the host will be mapped as 8080 on a container, and Port 8181 as 50000. I’ll not explain HTTP’s configuration on the host because it is not the subject of that article. The most important here is mounting /var/run/docker.sock from the host, as /var/run/docker.sock on the container. With that, our docker cli inside the container will communicate with the docker service on the host.
When we are ready, we run docker-compose build and docker-compose up. We connect to the container with docker-compose exec jenkins bash. Now inside the container, when we’ll run docker ps command, we should see all containers running on our host instead of containers running on our jenkins container. But when we run that command, we received permission denied error. Let’s fix it.
Solving docker.sock permission denied
In most cases, we run docker daemon with a specific user, and all other users do not have access to it.
Let’s start with the host configuration. In our case, we created user jenkins with sudo permissions. Let’s login as that user and run docker ps command. We should get permission denied on docker.sock. If we run that command with sudo, it will work. There is docker user’s group with required permissions, so we add jenkins user to that group and restart docker:
usermod -aG docker jenkins
sudo service docker restart
We need to logout and login again, and we’ll rerun docker ps. This time everything works on the host, but we still get the same error when we run docker ps on the container. Why?
Generally, container and host are two separate environments. All users created in both are also separate. If we want to have the same users, we need to configure them correctly. We already have jenkins user on host and container, but they are different users. A user is identified by uid, not username. So we need to make sure that both users have the same uid.
First, we need to verify uid on host:
```
> id -u jenkins
1004
```
As you see, we received uid — 1004. We’ll change our Dockerfile to reconfigure our jenkins user on the container. We do it by adding two lines in Dockerfile. First at the top of the file (after base image declaration) to define accepted arguments for build:
`ARG HOST_UID=1004`
Second, before we switch to jenkins user, to change user uid:
`RUN usermod -u $HOST_UID jenkins`
Next, we change our build definition in docker-compose.yml:
```
services:
    jenkins:
        build:
            context: ./
            args:
                HOST_UID: ${HOST_UID}
```
In the end, we add a new line in .env:
`HOST_UID=1004`
Now our uid is correct, but we still have a problem — why? Both host and container see our user as the same (they are still different users, but we cheated the user verification). Permissions are set for a user group, not a specific user. What more jenkins user in the container is not part of that group. We talk about the docker user’s group. That group in host and containers have the same name but different group id (the same problem as with user). First, we need to know gid for docker group on host:
```
>getent group | grep docker
docker:x:999:jenkins
```
The third part of the data contains gid — 999 in our case. Next, we adjust our Dockerfile, to make sure that the docker group has the same id on host and container and that jenkins user is in that group. First, we add a new argument to build:
`ARG HOST_GID=999`
Next, we change the group and modify user, before we switch to jenkins user:
```
RUN groupmod -g $HOST_GID docker
RUN usermod -aG docker jenkins
```
Again we change build definition in docker-compose.yml:
```
services:
    jenkins:
        build:
            context: ./
            args:
                HOST_UID: ${HOST_UID}
                HOST_GID: ${HOST_GID}
```
We also add correct variable in .env file:
`HOST_GID=999`
Now we build a docker image again and run the container (docker-compose build, docker-compose up). From now on, our Jenkins can run docker’s containers on the host without any issues.
If you can’t run a container with jenkins due permissions for Jenkins files, you need to adjust the files’ ownership on the host. As you remembered we messed a little bit with users ids and that may be an issue here. In our example, we need to run on host:
```
sudo chgrp -R /srv/www/jenkins jenkins
sudo chown -R /srv/www/jenkins jenkins
```
That would be all. With this configuration, we can run Jenkins in docker and use docker for builds. You can find Dockerfile and docker-compose.yml for presented example in here https://github.com/smoogie/jenkins_docker_example
