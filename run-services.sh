# Check docker command
if [ ! -x "$(command -v docker)" ]; then
  echo "Please install docker first!"
  exit 1
fi

# Check docker service
if ! docker info > /dev/null 2>&1; then
  echo "This script uses docker, and it isn't running - please start docker and try again!"
  exit 1
fi

# Source code
cd ~/
mkdir -p jenkins_home
mkdir -p gitlab_data/config
mkdir -p gitlab_data/logs
mkdir -p gitlab_data/data

mkdir -p jfrog_data/artifactory
sudo chown -R 1030 jfrog_data/

sudo rm -rf jenkins
git clone https://gitlab.com/longersoft-public/testing/install-jenkins.git jenkins

cd ./jenkins/
ln -snf ~/jenkins_home/ jenkins_home
ln -snf ~/gitlab_data/ gitlab_data
ln -snf ~/jfrog_data/ jfrog_data

# Prepare for config file
IP=$(curl ifconfig.me)
sed -i "s/localhost/$IP/g" ./gitlab/config/gitlab.rb

# Docker clean-up
docker kill $(docker ps -aq) > /dev/null 2>&1 && wait
docker stop $(docker container ls -aq) > /dev/null 2>&1 && wait
docker rm $(docker container ls -aq) > /dev/null 2>&1 && wait
#docker rmi $(docker images -aq) > /dev/null 2>&1 && wait
docker volume prune -f > /dev/null 2>&1 && wait
docker network prune -f > /dev/null 2>&1 && wait

# Build and run
sudo docker-compose down > /dev/null 2>&1
#docker-compose build && wait
sudo docker-compose up -d

#docker cp ./gitlab/config/gitlab.rb gitlab:/etc/gitlab/gitlab.rb
#docker-compose restart gitlab
