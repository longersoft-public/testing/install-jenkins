#!/bin/bash

# Cronj set shutdown EC2
date
# crontab -e
# 59 23 * * * root /usr/sbin/shutdown -h now
#sudo rm -f /var/spool/cron/crontabs/$USER
sudo -- sh -c "echo '59 23 * * * root /usr/sbin/shutdown -h now' > /var/spool/cron/crontabs/$USER"

cd /tmp/

# Install dependencies
sudo apt-get update -y && sudo apt-get upgrade -y

sudo apt-get install ca-certificates curl gnupg lsb-release -y
sudo rm -f /usr/share/keyrings/docker-archive-keyring.gpg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -snf /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version

# Fix docker permission
#sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker > /dev/null 2>&1 &
sudo chmod 666 /var/run/docker.sock
docker info

sudo apt-get install git -y

# Check docker command
if [ ! -x "$(command -v docker)" ]; then
  echo "Please install docker first!"
  exit 1
fi

# Check docker service
if ! docker info > /dev/null 2>&1; then
  echo "This script uses docker, and it isn't running - please start docker and try again!"
  exit 1
fi

# Source code
cd ~/
mkdir -p jenkins_home
mkdir -p gitlab_data/config
mkdir -p gitlab_data/logs
mkdir -p gitlab_data/data

mkdir -p jfrog_data/artifactory
sudo chown -R 1030 jfrog_data/

sudo rm -rf jenkins
git clone https://gitlab.com/longersoft-public/testing/install-jenkins.git jenkins

cd ./jenkins/
ln -snf ~/jenkins_home/ jenkins_home
ln -snf ~/gitlab_data/ gitlab_data
ln -snf ~/jfrog_data/ jfrog_data

# Prepare for config file
IP=$(curl ifconfig.me)
sed -i "s/localhost/$IP/g" ./gitlab/config/gitlab.rb

# Docker clean-up
docker kill $(docker ps -aq) > /dev/null 2>&1 && wait
docker stop $(docker container ls -aq) > /dev/null 2>&1 && wait
docker rm $(docker container ls -aq) > /dev/null 2>&1 && wait
docker rmi $(docker images -aq) > /dev/null 2>&1 && wait
docker volume prune -f > /dev/null 2>&1 && wait
docker network prune -f > /dev/null 2>&1 && wait

# Build and run
sudo docker-compose down > /dev/null 2>&1
docker-compose build && wait
sudo docker-compose up -d

docker cp ./gitlab/config/gitlab.rb gitlab:/etc/gitlab/gitlab.rb
docker-compose restart gitlab

# Clean up, again
#docker rmi hello-world --force
#docker rmi gitlab/gitlab-ce
#docker rmi jenkins/jenkins:lts
#docker rmi docker.bintray.io/jfrog/artifactory-cpp-ce
